package wire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {
	public static void main(String args[]){
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("wire/bean.xml");
		Educator educator=(Educator)applicationContext.getBean("b1") ;
		System.out.println(educator.getEmployee().getName());
	}

}
