package wire;

public class Educator {
	private Employee employee ;

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Educator(Employee employee){
		this.employee=employee ;
	}
	public Educator(){
		
	}
}
