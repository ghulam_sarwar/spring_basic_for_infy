package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("b1")
public class Employee {
	
	@Autowired
	@Value("Ghulam")
	private String name ;
	@Autowired
	@Value("10023")
	private int employeeId ;
	

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}


	public Employee() {
		// TODO Auto-generated constructor stub
	}

}
