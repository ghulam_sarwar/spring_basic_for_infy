package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("edu")
public class Educator {
	@Autowired
	private Employee employee ;

	public Educator() {
		// TODO Auto-generated constructor stub
	}
	
	public Educator(Employee e){
		employee=e ;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}
