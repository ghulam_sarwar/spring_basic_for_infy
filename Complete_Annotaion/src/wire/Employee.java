package wire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component("b2")
public class Employee {
	private String name ;
	private int employeeId ;

	public String getName() {
		return name;
	}
	@Autowired
	
	public void setName(@Value("Ghulam")String name) {
		this.name = name;
	}
	
	public int getEmployeeId() {
		return employeeId;
	}
	@Autowired
	public void setEmployeeId(@Value("3245")int employeeId) {
		this.employeeId = employeeId;
	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

}
