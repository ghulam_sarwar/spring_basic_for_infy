package wire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {

	public Hello() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("wire/bean.xml");
		Educator educator=(Educator)applicationContext.getBean("b1");
		System.out.println(educator.getEmployee().getEmployeeId());

	}
	

}
