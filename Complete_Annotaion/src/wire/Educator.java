package wire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
@Component("b1")
//@Service for Service component of business class
//@Repository for Dao class
//@Controller for controller class
public class Educator {
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Autowired
	@Qualifier("b2")  //autowire by name / absent of qualifier is autowire by type.
	private Employee employee;

	public Educator() {
		// TODO Auto-generated constructor stub
	}

}
