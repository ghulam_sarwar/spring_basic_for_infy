package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home.htm")
public class Home {
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView mappingGet(){
		ModelAndView andView =new ModelAndView();
		User user=new User();
		andView.addObject("user",user);
		andView.setViewName("home");
		
		return andView;
	}
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView mappingPost(@ModelAttribute("user")User user){
		ModelAndView andView=new ModelAndView();
		andView.addObject("user", user);
		System.out.println(user.getAddress());
		andView.setViewName("logIn");
		return andView ;
	}

}
