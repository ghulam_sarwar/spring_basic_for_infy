<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body >
	<form:form action="home.htm" acceptCharset="UTF-8" commandName="user" autocomplete="true" method="post" >
		Name : <form:input type="text" path="name" /><br>
		Address : <form:input type="text" path="address"/><br>
		Department : <form:input type="text" path="dept"/><br>
		Telephone : <form:input type="text" path="telephoneNumber"/><br>
		Submit : <input type="submit" />
	</form:form>

</body>
</html>