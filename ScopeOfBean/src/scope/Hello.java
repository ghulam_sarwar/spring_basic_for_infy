package scope;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("scope/bean.xml");
		System.out.println((Employee)applicationContext.getBean("b1"));
		System.out.println((Employee)applicationContext.getBean("b1"));
		System.out.println((Employee)applicationContext.getBean("b2"));
		System.out.println((Employee)applicationContext.getBean("b2"));

	}

}
