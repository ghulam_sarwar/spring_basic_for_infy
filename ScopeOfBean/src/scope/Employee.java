package scope;

public class Employee {
	public String name ;
	public int employeeId ;
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	public Employee(String name,int employeeId){
		this.name=name;
		this.employeeId=employeeId ;
	}
	

}
