package injection;

public class Educator {
	private String educator ;
	private String id;
	public String getEducator() {
		return educator;
	}
	public void setEducator(String educator) {
		this.educator = educator;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Educator() {
		// TODO Auto-generated constructor stub
	}

}
