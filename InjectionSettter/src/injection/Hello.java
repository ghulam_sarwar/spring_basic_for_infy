package injection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("injection/bean.xml");
//		Employee employee=(Employee)applicationContext.getBean("b1");
//		System.out.println(employee.getEmployeeId()+"::"+employee.getName());
//		
		Educator educator=(Educator)applicationContext.getBean("b2");
		System.out.println(educator.getId()+"::::::"+educator.getEducator());
	}

}
