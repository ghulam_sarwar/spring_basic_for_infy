package injection;

public class Employee {
	private String name ;
	private int employeeId ;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public Employee(){
		
	}

	public Employee(String name,int empid) {
		// TODO Auto-generated constructor stub
		this.name=name;
		employeeId=empid ;
	}

}
