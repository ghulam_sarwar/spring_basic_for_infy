package wire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {

	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context=new ClassPathXmlApplicationContext("wire/bean.xml");
		Educator educator=(Educator)context.getBean("b1");
		System.out.println(educator.getEmployee().getEmpId()+"::"+educator.getEmployee().getName());
	}

}
