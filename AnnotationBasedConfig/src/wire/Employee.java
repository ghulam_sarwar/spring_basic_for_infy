package wire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

		
public class Employee {
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmpId() {
		return empId;
	}
	@Autowired
	public void setEmpId(@Value("2324")String empId) {
		this.empId = empId;
	}
	@Autowired
	@Value("Sarwar")
	private String name ;
	private String empId ;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

}
