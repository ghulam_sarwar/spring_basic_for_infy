package wire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
	

public class Educator {
	private Employee employee ;
	
	
	public Employee getEmployee() {
		return employee;
	}
	/*
	@Autowired
	//by defaut is an autowire method
	 
	 */
	
	@Autowired
	@Qualifier("b2")
	//It is an autowire by name
	
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public Educator() {
		// TODO Auto-generated constructor stub
	}
	
}
