package setter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Hello {

	public static void main(String args[]){
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("setter/bean.xml");
		
		Employee employee=(Employee)applicationContext.getBean("b2");
		System.out.println(employee.getEducator().getName());
		
		
	}

}
