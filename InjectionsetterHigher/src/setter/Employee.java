package setter;

public class Employee {
	private Educator educator ;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	public Employee(Educator educator){
		this.educator=educator ;
	}
	public Educator getEducator() {
		return educator;
	}
	public void setEducator(Educator educator) {
		this.educator = educator;
	}

}
