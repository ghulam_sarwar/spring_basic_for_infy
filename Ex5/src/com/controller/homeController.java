package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class homeController {
	@RequestMapping("/home.htm")
	public void homePage(Model map){
		User arg1=new User();
		arg1.setAddress("Jhow Tala Road");
		arg1.setName("Ghulam Sarwar");
		map.addAttribute("user", arg1);
		map.addAttribute("home", "home");
	}

}
