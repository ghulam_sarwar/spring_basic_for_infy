package com.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	@RequestMapping("/home.htm")
	public  void home(ModelMap andView){
		User user=new User();
		user.setAddress("Jhow Tala Road Kolkata 19");
		user.setCity("Kolkata");
		user.setName("Ghulam Sarwar");
		user.setPinCode(700017);
		User user2=new User();
		user2.setAddress("Kimber Street");
		user2.setCity("Patna");
		user2.setName("Samad");
		user2.setPinCode(1234567);
		List<User> list=new ArrayList<User>();
		list.add(user);
		list.add(user2);
		andView.addAllAttributes(list);
		andView.addAttribute("message", "home");
		}

}
