package wire;

public class Educator {
	private Employee employee ;

	public Educator() {
		// TODO Auto-generated constructor stub
	}
	public Educator(Employee employee){
		this.employee=employee ;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
