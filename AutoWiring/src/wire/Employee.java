package wire;

public class Employee {
	
	private String name ;
	private int employeeId ;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	public Employee(String name ,int empId){
		this.name=name ;
		employeeId=empId ;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

}
