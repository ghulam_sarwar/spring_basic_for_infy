package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	@RequestMapping("/home.htm")
	public String homeGet(ModelMap map){
		User user=new User();
		map.addAttribute("user", "hello ghulam");
		return "home" ;
	}

}
