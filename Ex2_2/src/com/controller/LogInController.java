package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

@Controller
public class LogInController {
	
	@RequestMapping("/logIn.htm")
	public ModelAndView logIn(){
		
		ModelAndView andView=new ModelAndView();
		andView.setViewName("logIn");
		User user=new User();
		andView.addObject("user", user);
		andView.setViewName("logIn");
		return andView;
	}

}
