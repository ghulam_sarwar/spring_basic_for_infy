package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Register {
	
	@RequestMapping("register.htm")
	public ModelAndView register(@ModelAttribute("user") User user){
		ModelAndView andView=new ModelAndView();
		andView.addObject(user);
		System.out.println(user.getClass());
		andView.setViewName("register");
		
		return andView ;
	}

}
